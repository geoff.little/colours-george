﻿using System;

namespace Colours.Core.Models
{
    public class Colour
    {
        public int ColourId { get; set; }
        public string Name { get; set; }
        public bool IsEnabled { get; set; }
    }
}
