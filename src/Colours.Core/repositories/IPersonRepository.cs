﻿using System;
using System.Collections.Generic;
using System.Text;
using Colours.Core.Models;

namespace Colours.Core
{
    public interface IPersonRepository
    {
        Person Get(int id);
        IEnumerable<Person> GetAll();
        Person Update(Person person);
    }
}
