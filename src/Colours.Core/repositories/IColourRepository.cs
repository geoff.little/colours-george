﻿using System;
using System.Collections.Generic;
using System.Text;
using Colours.Core.Models;

namespace Colours.Core
{
    public interface IColourRepository
    {
        Colour GetColour(int id);
        IEnumerable<Colour> GetAll();
        Colour Update(Colour colour);
    }
}
